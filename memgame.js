/* memgame.js */


/*
level - what level they are on right now
speed - how long each number is shown
max - the highest number that will be shown
min - the lowest number that will be shown
cycles - how many numbers to show per level
*/
// defaults
	var level;
	var speed;
	var max;
	var cycles;

	var tick = 0; // stores the curent count
	var answer = 0; // stores the answer as it ticks along



function init(){
	console.log('init');
	//hide buttons until we need them
	$('button').hide();
	$('.notes').html('<p>Get ready to add up the numbers in your head as they flash on the screen</p>');

	//reset numbers
	level = 1;
	speed = 1500;
	max = 1;
	cycles = 6;

	tick = 0;
	answer = 0;

	$('.start').fadeIn(300).click(function(){start();});
}

function nextLevel(){
	console.log('nextLevel');
	$('button').hide();

	tick = 0;
	answer = 0;
	level ++;
	speed = speed - level * 10;
	if(speed < 400){
		speed = 400;
	}
	max = max + level/6;
	cycles = Math.floor(cycles + (cycles/4));
	console.log('speed: '+speed+', max'+max+', cycles'+cycles);
	start();
}


//tick, randomise, find and show the next number
function nextNumber(){
	tick++;
	console.log('nextNumber');
	
	var percent = tick/cycles*100;
	console.log(percent);
    $('.progress span').animate({width: percent+'%'}, speed-30 );

	var num = Math.ceil(Math.random()*max);
	console.log('tick -', tick);
	console.log('number -', num);
	$('.number').fadeOut(100, function(){
		$('.number').html('+'+num).fadeIn(200);
	});
	answer = answer + num;
	console.log('answer -', answer);

	$('.timer span').css('width', '0').animate({width: "100%"}, speed,
		function(){
			if (tick < cycles){
				nextNumber();
			} else {
				setTimeout(function(){
					console.log('finished level');
					$('.number').fadeOut(500, function(){
						$('.number').html('&nbsp;').fadeIn(0, function(){
							$('.timer span, .progress span').animate({width: "0"}, 100);
							var userAnswer = prompt('What is the total?');
							if (userAnswer == answer){
								youWin();
							} else {
								youLose(userAnswer);
							}
						});
					});
				},speed);
			}
		});
	
}

function youWin(){
	console.log('youWin');
	$('.notes').html('<p><strong>Level up!</strong><br />You are now on level '+(level+1)+'<br /></p>');
	$('.next').fadeIn(400).unbind('click').click(function(){nextLevel();});
}

function youLose(userAnswer){
	console.log('youLose');
	$('.notes').html('<p><strong>YOU LOSE!</strong><br />The answer was '+answer+'. You were out by '+Math.abs(userAnswer - answer)+'.</p><p>You made it to level '+level+' before you sucked.</p>');
	$('.restart').fadeIn(400).unbind('click').click(function(){init();});
}

// start the current level
function start(){
	console.log('start');
	$('.start').fadeOut(200);
	$('.number').html('ready?').fadeIn(200, function(){
		$('.number').delay(800).fadeOut(400, function(){
			nextNumber();
		});
	});
}

// Kick it off
init();